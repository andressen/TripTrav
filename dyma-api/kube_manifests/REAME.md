# Triptrav api ⇒ Kube(minikube) Docker, Argo for app  in local

```bash
**To made run locally the triptrav api follow this process**

Containeraize App (follow the docker pages to containeraize app)
```

[[TripTrav-api], Container to run locally and deploy to cloud run](https://www.notion.so/TripTrav-api-Container-to-run-locally-and-deploy-to-cloud-run-2330bf91964440b98b0c8bd8e916efff) 

Next, use minikube to made it running locally in cluster

```bash
Follow this tuto, it's perfect
```

[How To Run Flask REST API with minikube Kubernetes Cluster in Virtualbox](https://thecodinginterface.com/blog/flask-rest-api-minikube/)

```bash
Use kubens to set namespace and kubectl commands
After follow the tuto
kubectl get all

All the pods will not works properlly.
This is just because, the pods who runs the container got error of google credentials
You can check the logs with the command
=> kubectl logs -l[for the label] app=triptrav-api --all-containers=true
Bref, cette commande vous montrera que l'execution des containers n'a pas marché
à cause du google credential qu'il n'arrive pas à get
```

Pour resoudre ce problème:

follow ce tuto ⇒ 

[https://github.com/kubernetes/minikube/issues/9651](https://github.com/kubernetes/minikube/issues/9651)

```bash
Solution is just to run the command
=> minikube addons enable gcp-auth --alsologtostderr

Cette commande va insérer à l'intérieur de tes pods, les credentials de google
```

After it, all will works good

```bash
You can test by going see url 
```

For Argo

```bash

```