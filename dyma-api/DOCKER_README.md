# [TripTrav-api], Container to run locally and deploy to cloud run

```bash
Ce qu'il faut absolument savoir before begining

Processeur M1 de mac actuel => ARM
Pour faire fonctionner le code localement il faudrait containarizé en normal
puisque nous sommes en local

Une fois dans le folder api
=> docker build -t[tag] triptrav-api .
=> docker images
=> docker run -v "$HOME/.config/gcloud/application_default_credentials.json":/gcp/creds.json:ro --env GOOGLE_APPLICATION_CREDENTIALS=/gcp/creds.json -p 5000:5000 triptrav-api
Important de monter un volume pour faire passer les credentials docker si non$
cela ne fonctionnera pas et aussi, si en lancant l url ça ne fonctionne pas
bien vérifier que les ports sont libérer ou alors binder le port du container 
avec un celui de la machine de libre. -p 5000[celui de la machine]:5000[celui setter dans le code, donc qu utilise le container]

**NB: Une fois que tout fonctionne, tu peux le pousser dans ton hub ou alors le registry de google
mais faut savoir que pour run ce container chez google, t as besoin de le builder
avec un processseur AMD(le classque de chez intel quoi)

follow the part 2 =>** Workflow to deploy amd architecture container in gCloudRun using arm processor locally
```

First of all, when I downloaded sdk to run docker locally, I choose the one who could run properly with my processor ⇒ ARM PROCESSOR(M1 mac I got unfortunately or fortunately) 

1) For AMD and x86 processors (majority processor like intel and AMD...)
This is the most use case to do If we want to build our containers and move to run it in differents environments... 

Eg: Google cloud run untill now, does not accept container builded for ARM processor
He just accept container builded for amd or intel processor 

On the dyma-api folder

```bash
FROM python:3.8
# FROM python:3.9-slim
# FROM python:3.8-slim-buster
# FROM python:3.7-alpine

ENV PYTHONUNBUFFERED True

ENV APP_HOME /app

WORKDIR $APP_HOME

COPY . ./

RUN pip3 install -r requirements.txt

ENV PROJECT_ENV dev

CMD python3 main.py runserver 0.0.0.0:$PORT
```

Architecture

![Screenshot 2021-11-22 at 12.35.51.png](DOCKER_README/Screenshot_2021-11-22_at_12.35.51.png)

Commands:

```bash
docker build -t[tag] triptrav-api .
docker images # to reassure the image build successfully and exist locally
docker run triptrav-api 
```

Problem: An error occurred

```bash
google.auth.exceptions.DefaultCredentialsError: Could not automatically determine 
credentials. Please set GOOGLE_APPLICATION_CREDENTIALS or explicitly create 
credentials and re-run the application. For more information, 
please see https://cloud.google.com/docs/authentication/getting-started
```

Problem ⇒ How to load credentials in the container runtime environment for local tests

Solution ⇒ [Use google cloud user credentials when testing container](https://medium.com/google-cloud/use-google-cloud-user-credentials-when-testing-containers-locally-acb57cd4e4da) (user credential in running local container)

```bash
docker run -v "$HOME/.config/gcloud/application_default_credentials.json":/gcp/creds.json:ro --env GOOGLE_APPLICATION_CREDENTIALS=/gcp/creds.json -p 5000:5000 triptrav-api

In the code, on the main.py file, note that specify the host="0.0.0.0" is very important
Set this(host=) to '0.0.0.0' to
    have the server available externally as well

If we don't do it, the server containerize would not run.
```

To get a command line (CLI) in our terminal

```bash
docker ps => to see which container is running
docker exec -it [CONTAINER_ID] eec5e13baf80 /bin/sh

On the new terminal(linux architecture) you will see the triptrav-api folder
who containerize the project.
```

2) For ARM Processors localy and publish it on google cloud run

follow this instructions...

```docker
FROM python:3.8
# FROM python:3.9-slim
# FROM python:3.8-slim-buster
# FROM python:3.7-alpine

ENV PYTHONUNBUFFERED True

ENV APP_HOME /app

WORKDIR $APP_HOME

COPY . ./

RUN pip3 install -r requirements.txt

ENV PROJECT_ENV dev

CMD python3 main.py runserver 0.0.0.0:$PORT
```

![Screenshot 2021-11-26 at 14.17.21.png](DOCKER_README/Screenshot_2021-11-26_at_14.17.21.png)

.dockerignore

```docker
Dockerfile
README.md
*.pyc
*.pyo
*.pyd
__pycache__
.pytest_cache
venv3
dyma-mobile
*dyma-mobile
.gitignore
helloworld
.helloworld
```

# Workflow to deploy amd architecture container in gCloudRun using arm processor locally:

**Steps we will make to make gradually**

1. Build Container
2. Push on docker Hub or use Gcloud Build to built it directly and send it to Container registry or Google Cloud Artefacts
3. After push it on docker Hub, pull it locally
4. Tag the local container to create a new one with [gcr.io/gcp project/namesgeo/](http://gcr.io/dymatrip-dev/namesgeo/triptrav-api)[Project]
5. Check if tag is really in local (docker images)
6. Run it(the tag) to check if container works properly
7. Push it (he will go on google cloud container registry, or cloud artefacts)
8. And run it(hosted it) on google cloud run (make sure if you use secret manager on your code) make sure that the service account generated to manage cloud run has access to use secret manager)

```bash
# 1)&2) Build container for a specific type of architecture(by default on my machine
# he will build a arm container) and push directly on my docker hub.
# We are forced here to build an AMD platform cause, gcr just accept it not arm processor
# follow this link to understand all 
# => https://stackoverflow.com/questions/66823012/i-am-using-pycharm-and-cloud-run-plugin-to-deploy-flask-app-to-gcp-on-a-mac-m1-a
docker buildx build --platform linux/amd64 -t namesgeo/triptrav-api --push .

```

**To prepare buildx locally to specify platform follow the [stackoverflow](https://stackoverflow.com/questions/66823012/i-am-using-pycharm-and-cloud-run-plugin-to-deploy-flask-app-to-gcp-on-a-mac-m1-a) link**

Next

```bash
#3) After push it on docker Hub, pull it locally
docker pull namesgeo/triptrav-api

#4&5) tag it locally and Tag the local container to create a new one
docker images
docker tag namesgeo/triptrav-api gcr.io/dymatrip-dev/namesgeo/triptrav-api

#6)Run it(the tag) to check if container works properly
docker run -v "$HOME/.config/gcloud/application_default_credentials.json":/gcp/creds.json:ro --env GOOGLE_APPLICATION_CREDENTIALS=/gcp/creds.json -p8080:8080 namesgeo/triptrav-api:latest
# (En local, cette commande👆🏾 ne fonctionnera pas, tout simplement parce que 
# le conteneur a été buildé pour une plateforme AMD) or ma machine actuelle a 
# un processeur ARM) pour builder en arm, builder tout naturellement sans spécifier la plateforme
# ==> docker build -t test-img .
# Encore une fois, cela a été buildé pour AMD parce que gcloud n'accepte que 
# des containers buildé pour des platforms linux amd(le classik quoi...)

#7)Push it (he will go on google cloud container registry, or cloud artefacts)
docker push gcr.io/dymatrip-dev/namesgeo/triptrav-api

#8)And deploy it(hosted it) on google cloud run
gcloud run deploy --image gcr.io/dymatrip-dev/namesgeo/triptrav-api --port 8080 --region europe-west1 --allow-unauthenticated --platform managed
he wil prompt and ask you some info, like: => Service name (triptrav-api): pour le nom du service
aidera aussi à génerer l'url final e.g: https://triptrav-api-e22cijqu4q-ew.a.run.app/dyma-api/cities/
```

Pour [des problems d'accès à secret manager](https://stackoverflow.com/questions/62444867/secret-manager-access-denied-despite-correct-roles-for-service-account)

```bash
Dans IAM, attribuer le role **Secret Manager Secret Accessor** au service account
du compute généré pour faire run notre container sur GCP
```

![Screenshot 2021-11-26 at 14.40.29.png](DOCKER_README/Screenshot_2021-11-26_at_14.40.29.png)